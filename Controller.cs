﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_3semestr
{
    public class Controller: IController
    {
        IModel model;
        Random r = new Random();
        public IModel Model { get => model; set => throw new ArgumentException("Запрещено!"); }
        public Controller(MyModel m)
        {
            this.model = m;
        }
        public void Add()
        {
            model.AddNode(model.Count + 1);
        }
        public void Remove()
        {
            model.RemoveLastNode();
        }
        public void AddView(IView v)
        {
            v.Model = model;
            v.UpdateView();
            model.Changed += new Action(v.UpdateView);
        }
        public void HandlerNode(int x, int y)
        {
            //говорю model у какого Node c координатами x, y менять цвет 
            model.changeСolorNode(x, y);
        }


    }
}
