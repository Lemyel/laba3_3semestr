﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Laba3_3semestr
{
    public class MyModel : IModel
    {
        public event Action Changed;
        LinkedList<Node> nodes = new LinkedList<Node>();
        static Random r = new Random();
        public int Count => nodes.Count;// свойство Count возвращает nodes.Count
        public IEnumerable<Node> AllNodes { get => nodes; }

        static public Size MaxSize { get; set; }
        Pen[] cols = { Pens.Black, Pens.Green, Pens.Blue, Pens.Aquamarine, Pens.Pink };

        public void AddNode(int value)
        {
            //nodes.AddFirst(new Node(value, r.Next(MaxSize.Width-20), r.Next(MaxSize.Height-20) ));
            int i = r.Next(10), j = r.Next(10);
            int g = 0, h = 0;
            nodes.AddFirst(new Node(value,(i==g)? i: r.Next(10), (j == h) ? j : r.Next(10)));
            g = i; h = j;
            if (Changed != null) Changed();
        }
        public void RemoveLastNode()
        {
            if (nodes.Count == 0) return;
            nodes.RemoveFirst();
            //nodes.RemoveLast();
            if (Changed != null) Changed();
        }
        bool containsPoint(int x, int y, Node n)
        {
            return (Math.Pow((x - (n.X * 20 + 20 / 2)), 2) / Math.Pow(20 / 2, 2) + 
                Math.Pow((y - (n.Y * 20 + 20 / 2)), 2) / Math.Pow(20 / 2, 2)) <= 1;
        }

        Node findNode(int x, int y)
        {
            foreach (Node n in AllNodes)
            if (containsPoint(x, y, n))
            return n;
            return null;
        }

        public void changeСolorNode(int x, int y)
        {
            Node node = findNode(x, y);
            if (node != null) 
            {
                node.Color = cols[r.Next(cols.Length)];
                Node node2 = findNode(x+20, y+20);
                if (node2 != null) node2.Color = cols[r.Next(cols.Length)];
                Node node3 = findNode(x-20, y-20);
                if (node3 != null) node3.Color = cols[r.Next(cols.Length)];
            }
           if (Changed != null) Changed();
        }
    }
}
