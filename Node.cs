﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_3semestr
{
     public class Node
    {
        int x, y, value;
        Pen color;
        public Node(int value, int x, int y) 
        {
            this.value = value;
            this.x = x;
            this.y = y;
            color = Pens.Red;
        }
        public int X => x;
        public int Y => y;

         public Pen Color
         {
             get => color;
             set => color = value;
         }

    }
}
