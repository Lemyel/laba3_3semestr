﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba3_3semestr
{
    public partial class MainForm : Form
    {
        private IController controller;

        public MainForm()
        {
            InitializeComponent();
            MyModel m = new MyModel();
            controller = new Controller(m);
            IView v = new LabelView(label1);
            controller.AddView(v);
            controller.AddView(panelView1);
            controller.AddView(myDataGrid1);
            MyModel.MaxSize = panelView1.ClientSize;
            panelView1.coordinatesNode += controller.HandlerNode;
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            controller.Add();
        }
        private void buttonRemove_Click(object sender, EventArgs e)
        {
            controller.Remove();
        }
        public void panel1_ClientSizeChanged(object sender, EventArgs e)
        {
            MyModel.MaxSize = panelView1.ClientSize;//Getting Size of a form
        }
    }
}
