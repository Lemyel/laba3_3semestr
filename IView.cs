﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_3semestr
{
    public interface IView
    {
        IModel Model 
        {
            get; set; // хранит модель
        } 
        void UpdateView(); // обновляет представление
    }
}
