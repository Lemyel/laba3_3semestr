﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_3semestr
{
    public interface IModel
    {
        event Action Changed;
        void AddNode(int value); //  создаёт новый узел
        void RemoveLastNode(); // удалить последний узел
        int Count { get; } // узнать количество узлов
        IEnumerable<Node> AllNodes { get; }
       void changeСolorNode(int x, int y);
    }
}
