﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba3_3semestr
{
    public class MyDataGrid : DataGridView, IView
    {
        public IModel Model { get; set; }

        public void UpdateView()
        {
            this.AutoGenerateColumns = true;
            this.DataSource = Model.AllNodes.ToArray();
        }
    }

}
