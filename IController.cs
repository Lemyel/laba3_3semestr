﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_3semestr
{
    public interface IController
    {
        IModel Model { get; set; }  // хранит модель
        void AddView(IView v);//  добавляет ещё один просмотр IView под управление контроллера
        void Add();// добавить узел к модели
        void Remove();// убрать узел модели
        void HandlerNode(int x, int y);
    }
}
