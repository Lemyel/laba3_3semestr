﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Laba3_3semestr
{
    class PanelView : Panel, IView
    {
        public delegate void NodeHandler(int x, int y);
        public event NodeHandler coordinatesNode; // Определение события
        public IModel Model { get; set; }
        public PanelView()
        {
           //this.MaximumSize = new System.Drawing.Size(200, 200);
        }
        public void UpdateView()
        {
            Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            if (Model == null) return;
            Graphics g = e.Graphics;
            foreach (Node n in Model.AllNodes)
            { 
              g.DrawEllipse(n.Color, n.X * 20, n.Y * 20, 20, 20);
              //g.DrawEllipse(Pens.Red, n.X , n.Y , 20, 20);
            }
        }
        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (coordinatesNode != null)
                coordinatesNode(e.X, e.Y);
        }

    }
}
